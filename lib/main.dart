import 'dart:async';
import 'dart:convert';
import 'dart:ui';

import 'package:clus_demo/Spot.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_cluster_manager/google_maps_cluster_manager.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MapSample());
  }
}

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  ClusterManager? _manager;
  final Completer<GoogleMapController> _controller = Completer();

  Set<Marker> markers = Set();

  List<Spot> items = [];
  List<dynamic> test = [];

  final CameraPosition _parisCameraPosition =
      const CameraPosition(target: LatLng(41.3844999, 2.1396213), zoom: 12.0);

  @override
  void initState() {
    super.initState();
    DefaultAssetBundle.of(context).loadString("assets/spots.json").then((json) {
      List<dynamic> response = jsonDecode(json);

      setState(() {
        items = (response).map((json) => Spot.fromJson(json)).toList();
        _manager = _initClusterManager();
      });
    });
  }

  ClusterManager _initClusterManager() {
    return ClusterManager<Spot>(
      items,
      _updateMarkers,
      markerBuilder: _markerBuilder,
    );
  }

  void _updateMarkers(Set<Marker> markers) {
    setState(() {
      this.markers = markers;
    });
  }

  Future<Marker> Function(Cluster<Spot>) get _markerBuilder => (cluster) async {
        return Marker(
          markerId: MarkerId(cluster.getId()),
          position: cluster.location,
          onTap: () {
            print('---- $cluster');
            cluster.items.forEach((p) => print(p));
          },
          icon: await _getMarkerBitmap(cluster.isMultiple ? 125 : 75,
              text: cluster.isMultiple ? cluster.count.toString() : null),
        );
      };

  Future<BitmapDescriptor> _getMarkerBitmap(int size, {String? text}) async {
    final PictureRecorder pictureRecorder = PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);
    final Paint paint1 = Paint()..color = Colors.orange;
    final Paint paint2 = Paint()..color = Colors.white;

    canvas.drawCircle(Offset(size / 2, size / 2), size / 2.0, paint1);
    canvas.drawCircle(Offset(size / 2, size / 2), size / 2.2, paint2);
    canvas.drawCircle(Offset(size / 2, size / 2), size / 2.8, paint1);

    if (text != null) {
      TextPainter painter = TextPainter(textDirection: TextDirection.ltr);
      painter.text = TextSpan(
        text: text,
        style: TextStyle(
            fontSize: size / 3,
            color: Colors.white,
            fontWeight: FontWeight.normal),
      );
      painter.layout();
      painter.paint(
        canvas,
        Offset(size / 2 - painter.width / 2, size / 2 - painter.height / 2),
      );
    }

    final img = await pictureRecorder.endRecording().toImage(size, size);
    final data = await img.toByteData(format: ImageByteFormat.png) as ByteData;

    return BitmapDescriptor.fromBytes(data.buffer.asUint8List());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _manager != null
          ? GoogleMap(
              mapType: MapType.normal,
              initialCameraPosition: _parisCameraPosition,
              markers: markers,
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
                _manager?.setMapId(controller.mapId);
              },
              onCameraMove: _manager?.onCameraMove,
              onCameraIdle: _manager?.updateMap)
          : const CircularProgressIndicator(),
    );
  }
}
