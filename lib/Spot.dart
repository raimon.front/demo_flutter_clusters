// ignore_for_file: non_constant_identifier_names

import 'package:google_maps_cluster_manager/google_maps_cluster_manager.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Location {
  final double lat;
  final double lon;

  Location({required this.lat, required this.lon});

  factory Location.fromJson(Map<String, dynamic> json) {
    return Location(lat: json['lat'], lon: json['lon']);
  }
}

class SpotType {
  final int spot_type_id;
  final String name;
  final String description;

  SpotType(
      {required this.spot_type_id,
      required this.name,
      required this.description});

  factory SpotType.fromJson(Map<String, dynamic> json) {
    return SpotType(
      spot_type_id: json['spot_type_id'],
      name: json['name'],
      description: json['description'],
    );
  }
}

class Occupation {
  final int max_capacity;

  Occupation({required this.max_capacity});

  factory Occupation.fromJson(Map<String, dynamic> json) {
    return Occupation(max_capacity: json['max_capacity']);
  }
}

class Spot with ClusterItem {
  final int pom_id;
  final String name;
  final String address;
  final int location_type_id;
  final Location locationX;
  final SpotType spot_type;
  final Occupation occupation;

  Spot(
      {required this.pom_id,
      required this.name,
      required this.address,
      required this.location_type_id,
      required this.locationX,
      required this.spot_type,
      required this.occupation});

  @override
  LatLng get location => LatLng(locationX.lat, locationX.lon);

  factory Spot.fromJson(Map<String, dynamic> json) {
    return Spot(
      pom_id: json['pom_id'],
      name: json['name'],
      address: json['address'] ?? '',
      location_type_id: json['location_type_id'],
      locationX: Location.fromJson(json['location']),
      spot_type: SpotType.fromJson(json['spot_type']),
      occupation: Occupation.fromJson(json['occupation']),
    );
  }
}
